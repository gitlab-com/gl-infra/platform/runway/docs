FROM node:lts-alpine AS build
WORKDIR /app

COPY . .

RUN npm install
RUN npm run build

FROM httpd:2.4 AS runtime
COPY --from=build /app/dist /usr/local/apache2/htdocs/
COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf
EXPOSE 8080