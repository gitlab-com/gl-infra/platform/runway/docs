import { defineConfig } from 'astro/config'
import starlight from '@astrojs/starlight'
import starlightLinksValidator from 'starlight-links-validator'

// https://astro.build/config
export default defineConfig({
  integrations: [
    starlight({
      title: 'Runway Docs',
      social: {
        gitlab: 'https://gitlab.com/gitlab-com/gl-infra/platform/runway',
      },
      sidebar: [
        {
          label: 'Welcome',
          autogenerate: { directory: 'welcome' },
        },
        {
          label: 'Supported Runtimes',
          autogenerate: { directory: 'runtimes' },
        },
        {
          label: 'Managed Services',
          autogenerate: { directory: 'managed_services' },
        },
        {
          label: 'Unmanaged Services',
          autogenerate: { directory: 'unmanaged_services' },
        },
        {
          label: 'Guides',
          autogenerate: { directory: 'guides' },
        },
        {
          label: 'Reference',
          autogenerate: { directory: 'reference' },
        },
        {
          label: 'Runway Team',
          autogenerate: { directory: 'team' },
        },
      ],
      customCss: [
        './src/styles/custom.css',
      ],
      plugins: [starlightLinksValidator()],
    }),
  ],

  redirects: {
    '/guides/onboarding': '/welcome/onboarding',
    '/guides/offboarding': '/welcome/offboarding'
  },
});
