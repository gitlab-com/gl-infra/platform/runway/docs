# Environment Variables

Runway will automatically detect environment variables set in your source project and inject environment variables in your ingress container. The location of the files for staging and production are `.runway/env-staging.yml` and `.runway/env-production.yml`, respectively.

**WARNING**: Do _not_ use environment variables for secrets. Secrets management must be stored securely in [Vault](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/secrets-management.md?ref_type=heads) instead.

## Format

Environment variables must be represented as double-quoted strings in YAML, e.g. `"staging"`, `"8080"`, `"true"`, etc. Environment variables must be use `KEY: VALUE` convention, e.g. `FOO: "BAR"`.

Note that YAML allows for multi-line strings. This allow users to define configuration files as environment variables but limited to a [maximum variable size of 32Kb](https://cloud.google.com/run/quotas):

 ```
CONFIG_TOML: |
  [[bar]]
  id = 100
  name = "foo-1"
  address = "my.foo-1.example.com"

  [[serve]]
  address = ":8080"
  features = ["*_rest"]
```

## Add environment variable

To add environment variable, add new value in `.runway/env-*.yml`:

```yaml
# .runway/env-staging.yml
---
EXAMPLE_API_URL: "https://example.com/api/v1"
```

## Update environment variable

To update an environment variable, change the existing value in `.runway/env-*.yml`:

```yaml
# .runway/env-staging.yml
---
EXAMPLE_API_URL: "https://staging.example.com/api/v1"
```

## Remove environment variable

To remove an environment variable, remove the existing value in `.runway/env-*.yml`:

```yaml
# .runway/env-production.yml
---
```

Any changes will not be reflected until next deployment.
