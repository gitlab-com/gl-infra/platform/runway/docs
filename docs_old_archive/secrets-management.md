# Expose Secrets to a Runway Service

In order to securely expose secrets to a Runway service, you must place the secrets into the GitLab [Vault installation](https://vault.gitlab.net/) in a
specific format. The Runway deployment tooling/pipeline will then automatically make those secrets available as environment variables to the
containers in your environement.

## Setup

Right now, prerequisite for secrets management is access to Vault. Follow these steps:

1. Create AR to provision Okta group ([Example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/24997))
1. Create MR to set secret policy in Vault ([Example](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/7166))

When complete, service owners will only be granted access to secrets for their services.

**Recommendation**: Step 1 can take ~2 weeks for IT due to [change management](https://gitlab.com/gitlab-com/business-technology/change-management/-/issues/765), so start the process as soon as you anticipate the need for secrets management.

### Adding a secret to a Runway container in an Environment

1. Determine your `runway_service_id` by following the steps at https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/onboarding-new-service.md?ref_type=heads#2-check-the-generated-project

2. Log into Vault at https://vault.gitlab.net. If you do not have access to Vault, follow the instructions [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/access.md?ref_type=heads). The path you wish to have access to in vault is `runway/*/service/$runway_service_id`. The `$runway_service_id` is determined in step 1.

3. Once logged into Vault, navigate to `runway/$environment/service/$runway_service_id` where `$environment` is the environment you wish to add the secret to (typically `staging` or `production`) and `$runway_service_id` is the value you have determined above. You should see a single empty secret called `.placeholder`

4. Click on the `Create secret` button. At `Path for this secret` append the name you wish to use for the secret. Note that the name is also what will be used as the environment variable key inside the container, so you typically want this to be upper case and have letters/numbers/`_` only. Under `Secret data` put the word `value` on the left as the key, and then the contents of the secret you wish to add in the field next to it. Click `Save`

5. Run a normal Runway deployment and your secret will now be exposed to the container

### Rotating a secret

1. Log into Vault and go to the secret path for the environment/runway service you are interested in. Then click on the existing secret you wish to change.

2. Click on `Create new version` in the top right. You will see the contents of the secret (`value` and the masked secret value). Change the masked contents to the new data you wish to have for the secret, then click `Save`

3. Run a normal Runway deployment and the new contents of your secret will now be exposed to the container

### Removing a secret

1. Log into Vault and go to the secret path for the environment/runway service you are interested in.

2. Click on the `...` on the right next to the secret, and choose `Permanently delete`

3. Run a normal Runway deployment and the secret will no longer be exposed to the container
