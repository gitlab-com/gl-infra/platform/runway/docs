---
title: BYO GCP Folder
---

import { Aside, Steps } from '@astrojs/starlight/components';

## What does _BYO GCP Folder_ mean?

BYO (**B**ring **Y**our **O**wn) GCP (**G**oogle **C**loud **P**latform) Folder is a Runway feature that allows you to deploy your Runway workloads to your own GCP folder. By default, Runway will deploy to `gitlab-runway-<environment>` and these projects live in the GitLab.com organization, however you can also configure Runway to deploy to your own GCP folder, which may live in a different organization.

## Process

<Steps>

1. **Ensure the role `roles/billing.user` is granted to the provisioner SA on the billing account**

    You need to ensure the provisioner SA (`provisioner@gitlab-runway-production.iam.gserviceaccount.com`) has the role `roles/billing.user` for the `billing_account` that you will define in `inventory.yml`.

    :::note
    This is already done (via a Rackspace [support ticket](https://manage.rackspace.com/tickets/details/240701-ord-0000134)) for the billing account **1173105-US-GitLab** (`017B02-778F9C-493B83`) so you can skip this step if you are using this billing account.
    :::

2. **Create GCP folder where Runway will create projects**

    You need to create the folder(s) where you would like Runway to create projects. In Terraform, it would look like this:

    ```terraform
    resource "google_folder" "runway_services_folder" {
      display_name = "Runway"
      parent       = google_folder.your-parent-folder.name
    }
    ```

3. **Grant necessary roles to the provisioner at the GCP folder level**

    For the folder(s) you created in the previous step, ensure the roles `roles/resourcemanager.projectCreator` and `roles/billing.projectManager` have been granted to the provisioner (`provisioner@gitlab-runway-production.iam.gserviceaccount.com`).

    In Terraform, it would look like this:

    ```json {1-9, 14-25}
    locals {
      runway_provisioner_sa = "provisioner@gitlab-runway-production.iam.gserviceaccount.com"

      runway_folder_roles = [
        "roles/resourcemanager.projectCreator",
        "roles/billing.projectManager"
      ]
    }

    resource "google_folder" "runway_services_folder" {
      display_name = "Runway"
      parent       = google_folder.your-parent-folder.name
    }

    resource "google_folder_iam_member" "runway-folder-roles" {
      for_each = toset(local.runway_folder_roles)

      folder = google_folder.runway_services_folder.folder_id
      role   = each.key
      member = "serviceAccount:${local.runway_provisioner_sa}"

      depends_on = [
        google_org_policy_policy.allowed_policy_member_domains
      ]
    }
    ```

4. **Ensure Cloud Run services can allow unauthenticated requests**

    For each Runway service, we define an IAM policy that allows `allUsers` access to the `roles/run.invoker` role. Terraform will fail to apply this policy if you have DRS (**D**omain **R**estricted **S**haring) enabled with an error similar to:

    ```text
    Error 400: One or more users named in the policy do not belong to a permitted customer,  perhaps due to an organization policy
    ```

    There are a few ways to fix this, but the way we recommend is to **conditionally enforce DRS** as follows:

    1. Add a tag binding to the Runway GCP folder ([example](https://gitlab.com/gitlab-com/gl-infra/cells/tissue/-/merge_requests/163))
    1. Update your DRS organizational policy such that it will _exclude_ resources with specified tags from DRS ([part of the MR above](https://gitlab.com/gitlab-com/gl-infra/cells/tissue/-/merge_requests/163))
    1. Bind `roles/resourcemanager.tagAdmin` to the SA applying Terraform. This allows it to create new tags ([example](https://gitlab.com/gitlab-com/gl-infra/cells/tissue/-/merge_requests/166)).
    1. Bind `roles/resourcemanager.tagUser` to the SA applying Terraform. This allows it to bind tags ([example](https://gitlab.com/gitlab-com/gl-infra/cells/tissue/-/merge_requests/167)).

    See this [blog post](https://cloud.google.com/blog/topics/developers-practitioners/how-create-public-cloud-run-services-when-domain-restricted-sharing-enforced) for more info on this approach.

    :::note
    You can ignore this step for Cells as it has been done.
    :::

5. **Configure `gcp_project_groups` in provisioner's `inventory.yml`**

    You are now ready to configure the provisioner. Find the `gcp_project_groups` key in the [`inventory.yml`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml) file and add the following:

    ```yaml title="inventory.yml"
    - name: services
      envs:
        staging:
          folder_id: "<folder ID>"
          billing_account: "<billing account ID>"
          exporter_sa_email: "<exporter SA email>" # optional
    ```

    <Aside type="caution">
    Make sure you choose a short `name` as it will be part of the Google Project name, which is already prefixed with `gitlab-runway` and suffixed by a short version of the environment (e.g., `stg` or `prod`) so you really only have **11** characters to play with in the `name`.
    </Aside>

    Go ahead and merge this change and wait for it to be provisioned. Ensure you get a clean pipeline run with no errors.

    <Aside type="note">
    Due to the fact that the provisioner is creating Google Projects and enabling a number of API services, unfortunately Google does not immediately make those APIs available so if you see any errors due to API services not being available, simply wait a few minutes and retry the job. This is an unfortunate shortcoming that is not easy to solve as we are using Terraform, however this is only when creating GCP projects, which does not happen often.
    </Aside>

    If the `exporter_sa_email` field is empty in `inventory.yml`, the default exporter SA (`runway-exporter-k8s@gitlab-{production/staging-1}.iam.gserviceaccount.com`) will be granted the `roles/monitoring.viewer` role for the GCP project. Next, update [runway-exporter](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/8afc9adfcfefd8dd1d4979a7fc54d6c6a6dfce89/releases/runway-exporter/gprd.yaml.gotmpl#L4) to include the new GCP project id in the list of `--google.project-id` for runway-exporter to scrape stackdriver metrics.

    Alternatively, you may provide your own `exporter_sa_email` if you want to scrape metrics using a different set of runway-exporters.

6. **Create your Runway workload**

    You should now be able to add your workload to the `inventory.yml`. It is very similar to how you would add any other workload with the addition of `gcp_project_group`:

    ```yaml {3}
    - name: your-workload
      project_id: <your service project ID>
      gcp_project_group: services
    ```

</Steps>

## Limitations

1. We have VPC peering set-up between `gitlab-production` and `gitlab-runway-production` (and similarly for staging), which allows Rails to reach internal-only facing Runway workloads. By deploying to your own GCP folder, if you need to access your workloads via an internal-only load balancer, you will need to configure VPC peering yourself. Runway _does_ support creating VPC + subnetworks for internal load balancers. See the [schema](https://gitlab-com.gitlab.io/gl-infra/platform/runway/provisioner/inventory.schema.html#gcp_project_groups_items_envs_staging_vpc) for more info.

2. Deleting an entry from `gcp_project_groups`, applying it and attempting to add it again (with the same name) will **not** work. This is because once you apply the MR to delete the GCP project group, it will destroy all its resources including its IAM workload identity pool. These pools can be undeleted for up to 30 days before they are permanently deleted. Until then, **you cannot reuse the name**, so you cannot simply recreate the same GCP project group. You can either pick a new `name` OR you can manually undelete the workload identity pool and import the resources ([example](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/merge_requests/285)).
