---
title: Deployment Strategies
description: Chosing a deployment strategy
sidebar:
  order: 1
---

Runway implements multiple **Deployment Strategies**, which control how quickly and in which order canary revisions are deployed.

Services have different requirements for deployments.
For example, a service serving live user traffic may require a slow, region-by-region deployment, while a service in the early stages of development values the short turn-around times of an expedited deployment process.
During [incident response](/runtimes/cloud-run/incident-response/), an incident responder may need to roll back a service to the last known good configuration quickly.

## Available deployment strategies

* **regional_rollout:** This deployment strategy deploys canary revisions region-by-region sequentially,
  assigning 5%, 25%, 50%, 75% and finally 100% to the canary revision.
* **legacy:** This deployment strategy deploys canary revisions globally, i.e. to
  all regions simultaneously, assigning 25%, 50%, 75%, and finally 100% of
  traffic to the canary revision. It is the current default strategy.
* **expedited:** This deployment strategy deploys to 100% of traffic globally
  in one "big bang" deployment. It is intended for interactive use (e.g. during
  development) and [incident response](/runtimes/cloud-run/incident-response/).
* **dryrun:** The "dryrun" strategy does not make any changes to the system and
  is run in a merge request pipeline to preview changes.

## Setting a deployment strategy

You can set a deployment strategy in 2 ways:

1. Using the `RUNWAY_DEPLOYMENT_STRATEGY` CI variable. You can either set the variable in the project's CI settings, or using the [`variables`](https://docs.gitlab.com/ee/ci/yaml/#variables) global keyword.

    ```yaml
    variables:
      RUNWAY_DEPLOYMENT_STRATEGY: "expedited"
    ```

2. Configuring [`spec.deployment.strategy`](https://schemas.runway.gitlab.com/RunwayService/#spec_deployment_strategy) in `runway.yml`.

    ```yaml
    spec:
      deployment:
        strategy: "expedited"
    ```

   This allows you to set different strategies between staging and production environment. For example, you could set `expedited` on staging and `regional_rollout` on production.


:::note
The `RUNWAY_DEPLOYMENT_STRATEGY` job variable takes precedence over `spec.deployment.strategy` defined in `runway.yml`.
:::

## Setting traffic rollout percentages

:::note
This feature only applies to **legacy** and **regional_rollout** strategy.
:::

Instead of rolling out traffic using the default deployment steps as described in [the deployment strategies](#available-deployment-strategies). You can also configure the rollout percentages in the [`deployment.traffic_rollout_percentages`](https://schemas.runway.gitlab.com/RunwayService/#spec_deployment_then_rollout_traffic_percentages) in `runway.yml`. For example:

 ```yaml
 spec:
   deployment:
     strategy: "regional_rollout"
     traffic_rollout_percentages: [1, 10, 50, 100]
 ```

## Auto rollback

:::note
This feature only applies to **legacy** and **regional_rollout** strategy.
:::

From [v3.26.0](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/releases/v3.26.0), Runway automatically rolls back to the stable revision when the new canary revision is experiencing an elevated 5xx error ratio in one of the regions.

The rollback process is described as the following:

1. Right after the 25% deployment, Runway sleeps for 5 minutes (configurable by `LEGACY_DEPLOY_MONITOR_DELAY_DURATION_S` variable in your deployment project) to monitor and compare the canary vs stable revision error ratio for each region.
1. When a region's new canary revision error ratio is elevated by `50%` continuously for 5 minutes, all regions are rolled back to 100% traffic to the stable revision,
1. The deployment job fails eventually.

## Change Lock

Runway deployments adhere to production change lock process. To learn more, refer to [production change lock](/guides/production-change-lock/) page.
