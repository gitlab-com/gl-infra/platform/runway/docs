---
title: Production Change Lock
description: Production Change Lock and Deploy Freeze allow to pause Runway deployments temporarily.
---

Runway provides two mechanisms for pausing releases for a limited time:

* [Production Change Lock](https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl) (PCL):
  intended to halt changes to production during certain events such as GitLab Summit and major global holidays. PCL applies to an entire department or group.
* [Deploy Freeze](https://docs.gitlab.com/user/project/releases/#prevent-unintentional-releases-by-setting-a-deploy-freeze):
  intended to halt deployments of a specific service, for example to stop automation from overwriting a manual rollback.

## Production Change Lock

Runway deployments adhere to [Production Change Lock](https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl) (PCL) process for change management.
PCL uses "tags" to limit which department or group to lock. By default, Runway uses the `delivery` tag.
To set use another tag, set the `SOURCE_PROJECT_DEPLOY_FREEZE` CI variable in the service project.
Multiple tags must be comma separated, e.g. `deployment,infrastructure`.

### How it works

Runway relies on [change-lock](https://gitlab.com/gitlab-com/gl-infra/change-lock) tooling to check for PCL in deployment pipelines.
When a deployment is outside the changelock window, the check passes and deployment proceeds:

![Production Change Lock Check](../../../assets/img/pcl.png)

When a deployment is within the changelock window, the check fails and deployment is halted.

### Overriding an active change lock

Runway supports overriding the production change lock check by setting `CHANGE_LOCK_OVERRIDE` CI variable in [deployment](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments) project:

![Production Change Lock Check Override](../../../assets/img/pcl-override.png)

The ability for service owners to override for production services is not considered an exemption to hard PCL.
As per [handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/change-management/#hard-pcl), exemption requires approval of Sr. Director of Infrastructure Platforms or their designee.

## Deploy Freeze

Runway honors the [Deploy Freeze](https://docs.gitlab.com/user/project/releases/#prevent-unintentional-releases-by-setting-a-deploy-freeze) GitLab feature.
This feature allows pausing deployments for individual projects, e.g. to prevent Runway from overwriting a manual incident mitigation.

The deploy freeze can be configured in the service project as well as the deployment project.
We recommend to configure the deploy freeze in the service project, since it may stop other deployment tooling that might exist there.
Configure a deploy freeze in the deployment project only if you require other deployemnt tooling in the service project to run.

Deploy freezes can be configured in the *Settings → CI/CD* UI under the point *Deploy freezes*.
