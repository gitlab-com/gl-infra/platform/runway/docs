---
title: Production Readiness Review
description: How to streamline paved path for Runway services
---

A guide for streamlining production readiness review for Runway services.

## Context

SaaS Platforms uses [production readiness review](https://handbook.gitlab.com/handbook/engineering/infrastructure/production/readiness/) process for new services. Runway aims to provide production-readiness by default with [Frameworks SRE Engagement Model](https://sre.google/sre-book/evolving-sre-engagement-model/).

By using a paved path on Runway, service owners automatically meet majority of criteria.

## Process

1. [Create an issue](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/new?issuable_template=production_readiness_runway) using **Runway template** in the [readiness project](https://gitlab.com/gitlab-com/gl-infra/readiness).
1. Follow the Readiness Checklist in the **Runway template**.

For more information, refer to [handbook](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/new?issuable_template=production_readiness_runway).
