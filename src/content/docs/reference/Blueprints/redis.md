---
title: Memorystore for Redis
description: Architecture design document for Redis instances
creation-date: "2024-08-16"
authors: ["@cfeick"]
---

## Summary

Runway currently supports stateless services. Candidate services have requested support for stateful services. While stateless services were ideal for the first iteration, limitations are now preventing new services from onboarding and existing services from increasing adoption.

As part of strategy to support stateful services, the next logical iteration is application caching instances. Propose caching instance capabilities using [GCP Memorystore for Redis](https://cloud.google.com/memorystore/docs/redis/memorystore-for-redis-overview) to allow service owners to self-serve fully-managed cloud infrastructure.

## Motivation

The importance and urgency for Redis features, such as caching, session management, etc., is to remove key blockers for candidate services. Most notably, [CustomersDot](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/prdsub/main.tf#L285-312) and [AI Gateway](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/263#note_1943921731).

Many competing PaaS offerings include features for Redis instances. As a result, Runway has opportunities to provide Redis instances as part of the workloads deployed using Runway.

### Goals

- Service owners can self-serve Redis caching instances
- Support GCP Memorystore for Redis
- Compatibility with GCP Cloud Run Services
- Compatibility with GCP Cloud Run Services Multi-Region
- Compatibility with GCP Cloud Run Jobs
- Compatibility with GCP Google Kubernetes Engine (note: stretch goal, in progress)

### Non-Goals

- Service owners can self-serve Redis persistence instances (reason: no specific requirement, still feasible w/ proposal). As a result, Redis instances are not intended to withstand dataloss. When persistence instances are eventually supported, guarantees will be made for DR/RTO
- Support GCP Memorystore for Redis Cluster (reason: too complex for first iteration, refer to alternative solutions section)
- Support GCP Memorystore for Memcached (reason: not standardized at GitLab)

## Proposal

Propose managing the entire lifecycle of Redis caching instance capabilities, so service owners can self-serve using Runway.

![Redis architecture](../../../../assets/img/redis-architecture.png)

### Provision an instance

Service owners must be able to create and manage caching instance. In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), inventory is updated to support provisioning [GCP Memorystore for Redis instance](https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#resource:-instance) using Terraform, e.g. `google_redis_instance`.

### Configure an instance

Service owners must be able to configure caching instances. In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), support configuration for managing both Redis instance (e.g. memory size capacity) and Redis configuration (e.g. maxmemory policy, parameters).

### Connect to an instance

Service owners must be able to connect to caching instance from their service. In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), store caching instance host and credentials under service's path using Vault, e.g. `runway/env/staging/example-service`. In [Reconciler](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl), make caching instance credentials accessible to service using [GCP Cloud Run secrets management](https://cloud.google.com/run/docs/configuring/services/secrets#access-secrets).

Service owners must be able to connect to _multiple_ instances from a service, which is a common scenario for caching and persistence instances.

### Secure an instance

Service owners must be able to securely connect only to instances available in their service's secrets management. In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), update service accounts to use [predefined roles](https://cloud.google.com/memorystore/docs/redis/access-control#permissions_and_roles) offered by GCP Memorystore for Redis.

Service owners must use [authentication](https://cloud.google.com/memorystore/docs/redis/about-redis-auth#security_and_privacy) for caching instances, which will be enabled by default for all services and cannot be opted out.

Service owners must be able to enable [in-transit encryption](https://cloud.google.com/memorystore/docs/redis/about-in-transit-encryption) and [configure clients](https://cloud.google.com/memorystore/docs/redis/manage-in-transit-encryption).

### Monitor an instance

Service owners must be able to monitor caching instances. Create new Runway [`redis_exporter`](https://github.com/oliver006/redis_exporter) for each Runway Redis instance to to scrape metrics using Prometheus. In [Runbooks](https://gitlab.com/gitlab-com/runbooks/), use monitoring and capacity planning for GCP Memorystore for Redis.

### Deprovision an instance

Service owners must be able to disconnect caching instances from their service. Service owners must be able to destroy caching instances.

### Pricing for an instance

Service owners must be able to be attribute costs of caching instances. In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), attach standard resource labels to Redis instances using [GitLab Infrastructure standards](https://handbook.gitlab.com/handbook/infrastructure-standards/labels-tags/).

Historically, Runway's operating cost has been negligible due to GCP Cloud Run pricing. By introducing support for caching instance capabilities, this is no longer the case. For contrast, a single standard tier GCP Memorystore for Redis instance can cost $805.92/month.

Pricing is based on [components](https://cloud.google.com/memorystore/docs/redis/pricing) for tier, capacity, region, and replicas. When accessing a Redis instance from a Cloud Run Service client in a different region, Memorystore charges you for network egress traffic from Redis instances to your client application for total GB transferred from one region to the other.

Basic tier instances should be used for development and testing, and Standard tier instances should be used for [GA features](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#ga-features).

## Design and implementation details

### Fully-managed

As a fully-managed solution in GCP, scalability, availability, and maintaince are considered [features](https://cloud.google.com/memorystore/docs/redis/memorystore-for-redis-overview#features).

#### Scalability

Instances can be vertically scaled up to a maximum of 300 GB and supports up to 16 Gbps of network throughput. Instances can be horizontally scaled with read queries across up to five replicas. When scaling a Standard Tier instance, applications experience downtime of less than a minute.

Based on self-managed Redis instances for GitLab.com, CPU utilization saturation resource has been primary bottleneck. Horizontal scaling with read replicas are only suited for specific workloads and require trade-offs with availability. For long-term horizontal scaling solution, Runway will need to eventually support [GCP Memorystore for Redis Cluster](https://cloud.google.com/memorystore/docs/cluster/memorystore-for-redis-cluster-overview).

#### Availability

Instances can be provisioned as Standard Tier [types](https://cloud.google.com/memorystore/docs/redis/redis-tiers), which are replicated across zones, monitored for health and have fast automatic failover. According to GCP, Standard Tier instances also provide an SLA of 99.9%.

#### Maintenance

Instances can enable maintenance policy that is [routinely scheduled](https://cloud.google.com/memorystore/docs/redis/about-maintenance).

Runway will be responsible for defining a maintenance window for all services by default. Service owners will be responsible for optionally [overriding default maintenance policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/redis_instance.html#nested_maintenance_policy) depending on preference.

Runway will be responsible for using [capacity planning process](https://handbook.gitlab.com/handbook/engineering/infrastructure/capacity-planning/) and [maintenance notifications](https://cloud.google.com/memorystore/docs/redis/about-maintenance) annotations to ensure [system memory utilization](https://cloud.google.com/memorystore/docs/redis/about-maintenance#impact_of_maintenance) is rightsized for maintenance windows depending on workload instance traffic.

Service owners will be responsible for [exponential backoff](https://cloud.google.com/memorystore/docs/redis/exponential-backoff) to handle client reconnections after maintenance failover in non-LabKit supported programming languages. Runway will be responsible for implementing Redis client functionality in LabKit supported programming languages, on a just-in-time case-by-case basis.

### Data Model

`Workload` is a workload in GCP Cloud Run (e.g. service, job). `Instance` is a Redis instance of GCP Memorystore for Redis that will initially support type `CACHING` and can be extended to support type `PERSISTENCE`. `Workload` must be able to connect to multiple `Instance`s (e.g. caching, persistence). `Instance` must be able to be provisioned and configured to one or more environments (e.g. `staging`, `production`).

Assumption for 1:1 mapping may simplify the first iteration, however, it will not be as easily extendable for common scenarios, such as an instance being accessed by both a service and/or job, or a service connecting to multiple instances for caching and background job processing.

#### Terraform

In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), Runway must manage IaC for [`google_redis_instance`](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/redis_instance.html) resource.

Infrastructure has an [existing Terraform module](https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/memorystore-redis) to provision GCP Memorystore for Redis instance. Right now, outputs are limited to the private IP address of Redis instance and the module is only used for CustomersDot, so it does not make sense to extend it any further.

Due to Runway's multi-tenant PaaS use case, we can leverage [Google Cloud Memorystore Terraform Module](https://registry.terraform.io/modules/terraform-google-modules/memorystore/google/latest#outputs), which already includes outputs for `host`, `auth_string`, `read_endpoint`, etc.

#### JSON Schema

In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), Runway must include JSON Schema for caching instances. By introducing `type` property, Runway can both provide preconfigured defaults for `CACHE` type (e.g. `maxmemory`) and be easily extended to eventually support `PERSISTENCE` type configuration using [conditional required properties](https://json-schema.org/understanding-json-schema/reference/conditionals#ifthenelse), e.g. `rdb_snapshot_period`.

Below are a few illustrative examples.

Provisioning basic instance:

```json
{
  "redis_instances": [
    {
      "name": "redis-dev-instance",
      "tier": "BASIC",
      "type": "CACHE",
      "provider": "GCP",
      "environments": [
        "staging"
      ]
    }
  ]
}
```

Provisioning standard high availability instance:

```json
{
  "redis_instances": [
    {
      "name": "redis-beta-instance",
      "tier": "STANDARD_HA",
      "type": "CACHE",
      "provider": "GCP",
      "environments": [
        "staging",
        "production"
      ]
    }
  ]
}
```

Provisioning regional instance:

```json
{
  "redis_instances": [
    {
      "name": "redis-ga-instance",
      "tier": "STANDARD_HA",
      "type": "CACHE",
      "provider": "GCP",
      "regions": [
        "us-east1",
        "europe-west1"
      ],
      "read_replicas_enabled": true,
      "replica_count": 3,
      "memory_size_gb": 50,
      "environments": [
        "production"
      ]
    }
  ]
}
```

Configuring instance:

```json
{
  "redis_instances": [
    {
      "name": "redis-beta-instance",
      "tier": "STANDARD_HA",
      "type": "CACHE",
      "provider": "GCP",
      "environments": [
        "staging",
        "production"
      ],
      "redis_configs": {
        "maxmemory-policy": "noeviction"
      }
    }
  ]
}
```

Provisioning persistent instance:

```json
{
  "redis_instances": [
    {
      "$comment": "out of scope: illustrative example",
      "name": "redis-persist-instance",
      "tier": "STANDARD_HA",
      "type": "PERSISTANCE",
      "provider": "GCP",
      "rdb_snapshot_period": "ONE_HOUR"
    }
  ]
}
```

Additionally, service inventory will be extended to connect a service to instances. Here's an illustrative example:

```json
{
  "inventory": [
    {
      "name": "cloudrun-service",
      "project_id": 123456789,
      "redis_instances": [
        "redis-instance"
      ]
    }
  ],
  "redis_instances": [
    {
      "name": "redis-instance",
      "tier": "BASIC",
      "type": "CACHE",
      "provider": "GCP",
      "environments": [
        "staging",
        "production"
      ]
    }
  ]
}
```

As examples demonstrate, JSON Schema should be flexible enough to support provisioning and configuring any supported attributes, reguardless of instance type.

#### Multi-Region

By default, provisioning caching instance will use same default region as GCP Cloud Run Service, e.g. `us-east1`. Optionally, `regions` attribute can be set to provision a cache instance per region:

```json
{
  "redis_instances": [
    {
      "name": "redis-ga-instance",
      "tier": "STANDARD_HA",
      "type": "CACHE",
      "regions": [
        "us-east1",
        "europe-west1"
      ],
    }
  ]
}
```

Additionally, replication can be enabled for each regional cache instance:

```json
{
  "redis_instances": [
    {
      "name": "redis-ga-instance",
      "tier": "STANDARD_HA",
      "type": "CACHE",
      "regions": [
        "us-east1",
        "europe-west1"
      ],
      "read_replicas_enabled": true,
      "replica_count": 3
    }
  ]
}
```

Separate regional caches w/ regional read replication are supported, not global cache replicated across multiple regions.

### Integration

#### Vault

In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), Runway must store caching instance credentials, so it can be accessed by [Reconciler](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl).

Runway currently uses Vault for secrets management, which is workload and runtime agnostic. As a result, Vault will be the mechanism to connect to an instance.

Secrets for an instance will be stored under the following:

| Mount    | Path                                           |
|----------|------------------------------------------------|
| `runway` | `env/$RUNWAY_ENV/service/$RUNWAY_SERVICE_ID/runway_redis/$RUNWAY_REGION/*` |

Fields for an instance will include the following:

| Field         | Description                                                                                     |
|---------------|-------------------------------------------------------------------------------------------------|
| Password      | AUTH string for a Redis instance.                                                               |
| Host          | Hostname or IP address of the exposed Redis endpoint used by clients to connect to the service. |
| Port          | The port number of the exposed Redis endpoint.                                                  |
| Read Endpoint | Hostname or IP address of the exposed readonly Redis endpoint.                                  |
| Read Port     | The port number of the exposed readonly redis endpoint.                                         |

The service owner experience should be very similar to competing SaaS offerings, e.g. Heroku Redis add-ons. When a service is connected to a single instance, secrets will be prefixed with `RUNWAY_REDIS_`, e.g. `RUNWAY_REDIS_HOST`. When a service is connected to multiple instances, secrets will be suffixed with a unique identifier, e.g. `RUNWAY_REDIS_HOST_$ID`.

#### AUTH

When creating a Memorystore for Redis instance, Runway will enable [authentication](https://cloud.google.com/memorystore/docs/redis/about-redis-auth) by default and cannot be opted out.

The AUTH string is a UUID that Runway will retrieve using Terraform [module](https://registry.terraform.io/modules/terraform-google-modules/memorystore/google/latest) and store in Vault under service path, so service clients can retrieve for authenticating during connection.

To rotate AUTH string, authentication must unfortunately be temporarily disabled then [re-enabled](https://cloud.google.com/memorystore/docs/redis/manage-redis-auth#changing_the_auth_string). Once a [client authenticates with an AUTH string](https://cloud.google.com/memorystore/docs/redis/about-redis-auth#auth_behavior), it remains authenticated for the lifetime of that connection, even if you change the AUTH string. However, autoscaling new GCP Cloud Run instances, or new connections from application-side connection pooling will still result in using previous AUTH string. To reduce impact on availability, service owners must handle failing connections as supported by Redis client library, and re-deploy service immediately after rotation.

#### VPC

To connect to GCP Memorystore for Redis instance from GCP Cloud Run service, Runway must prepare [VPC network egress](https://cloud.google.com/memorystore/docs/redis/connect-redis-instance-cloud-run#egress).

Based on the constraints of Runway's multi-tenant PaaS use case, using [Serverless VPC Access Connector](https://cloud.google.com/vpc/docs/configure-serverless-vpc-access#cloud-run) should provide the simplest isolation by each service having a connector when using caching instance. In [Reconciler](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl), configure [`vpc_access`](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloud_run_v2_service#example-usage---cloudrunv2-service-vpcaccess) for service.

Worth noting: VPC will likely need to be revisited for compatibility with GCP Google Kubernetes Engine once runtime becomes more stable.

#### IAM

In [Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner), [`roles/redis.admin`](https://cloud.google.com/memorystore/docs/redis/access-control) is required for managing instances and GCP Cloud Run service accounts should not require updates roles due to network.

#### Metrics

Runway must offer SLIs, SLOs, dashboards, and saturation montioring for Redis caching instances based on [`redis_exporter`](https://github.com/oliver006/redis_exporter) metrics. In [Runbooks](https://gitlab.com/gitlab-com/runbooks/), use  [Redis archetype](https://gitlab.com/gitlab-com/runbooks/-/blob/master/libsonnet/service-archetypes/redis-archetype.libsonnet?ref_type=heads) for Runway Redis instances.

Runway must create a new Runway redis exporter to scrape Runway Redis instances and remote write to Mimir using existing `runway` tenant, similar to how existing [`stackdriver_exporter`](https://github.com/prometheus-community/stackdriver_exporter) scrapes Runway Cloud Run service [metrics](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/runway-exporter?ref_type=heads). [VPC peering](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/main/environments/runway-production/network.tf), firewall rules, and scrape configuration with multi-targets will need to be setup to access Runway Redis instances located in `gitlab-runway-*` GCP projects from remote write redis_exporter release located in `gitlab-*` GCP projects.

#### Constraints

In addition to non-goals, the following is unsupported:

- Runway connecting multiple services to single shared Redis instance
- Runway disaster recovery and recovery time objective for Redis caching instances
- GCP Memorystore data migration
- GCP Memorystore detailed performance analysis (e.g. CPU profiling, tcpdump traffic analysis, rdb memory space analysis, [etc](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/redis.md))

## Alternative Solutions

1. [Cloud Run Integrations](https://cloud.google.com/run/docs/integrate/redis-memorystore). Cloud Run Integrations is considered a Pre-GA feature, which is available "as-is" with limited support and not recommended for production usage.
1. [Direct VPC egress](https://cloud.google.com/run/docs/configuring/vpc-direct-vpc). Serverless VPC Access Connector provides service tenant isolation without complex network configuration.
1. [GCP Cloud Monitoring](https://cloud.google.com/memorystore/docs/redis/supported-monitoring-metrics). Prometheus Redis Exporter provides prior art from GitLab.com and GitLab Dedicated for metrics, dashboards, alerts, and saturation monitoring.
1. [Memorystore for Redis Cluster](https://cloud.google.com/memorystore/docs/cluster/memorystore-for-redis-cluster-overview). [Redis Cluster will be reassessed before adding support for persistence instances](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/merge_requests/64#note_2070563875).
