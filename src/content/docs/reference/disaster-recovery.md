---
title: Disaster Recovery
description: This describes the high level disaster-recovery architecture
---

## 🛑 Make It Stop!

There are two options to prevent Runway from making changes to production:

1. In the service project, set the `RUNWAY_DEPLOYMENT_ACTION` CI variable to `manual`.
   - In the service project, navigate to *Settings → CI/CD*
   - In the *Variables* section, use the *Add variable* button to create the variable.
2. Configure a [Deploy Freeze](https://docs.gitlab.com/user/project/releases/#prevent-unintentional-releases-by-setting-a-deploy-freeze).
   - In the service project, navigate to *Settings → CI/CD*
   - In the *Deploy freezes* section, use the *Add deploy freeze* button to create a new deploy freeze.

## Deployments from `ops.gitlab.net`

Deployments normally take place from gitlab.com, however certain projects (e.g.,
Cells [topology-service](https://gitlab.com/gitlab-org/cells/topology-service))
are critical to the availability of gitlab.com so to decouple any reliance
on gitlab.com, we support [deploying workloads from ops.gitlab.net](/guides/deploying-from-ops/) instead.

### Architecture Diagram

![Runway Ops Deployment Architecture Diagram](../../../assets/img/runway-ops-deployment-architecture.png)

Refer to the [migration guide](/guides/deploying-from-ops/) on how to set up service and deployment projects for ops deployments.

### Components

#### Push mirror

The push mirror is configured with a personal access token (PAT) from the `ops-gitlab-net` bot account. This is to allow commits created by the push mirror to trigger downstream pipelines in the deployment project.

Refer to the [migration guide](/guides/deploying-from-ops/) for more information on configuring the push mirror.

Refer to [this issue](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/360) for more background context.

#### ops.gitlab.net-specific resources

In the provisioner, workloads configured to deploy from ops will have the following additional resources created:

- [service account and iam member configured](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/modules/runway_service/ops_service_accounts.tf)
- [deployment project, vault and OIDC module](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/66677a59bf00146b23a8f9584d94a15b0fce4682/main.tf#L82)
- `RUNWAY_DEPLOY_HOST_PROJECT_ID` [GitLab project variable on the canonical deployment project](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/66677a59bf00146b23a8f9584d94a15b0fce4682/main.tf#L109)

In [infra-mgmt](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/main/environments/gitlab-com/project_runway_provisioner.tf), the following resources will be created:

- `RUNWAY_DEPLOY_HOST_PRAT` [GitLab project variable on the canonical deployment project](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/66923c91fc9eb540944d5de32d3d8727dbfd22e9/environments/gitlab-com/project_runway_provisioner.tf#L191)
- GitLab project variable and [project access token for the ops.gitlab.net deployment project](https://gitlab.com/gitlab-com/gl-infra/infra-mgmt/-/blob/66923c91fc9eb540944d5de32d3d8727dbfd22e9/environments/gitlab-com/project_runway_provisioner.tf#L174)

#### Terraform state backend

The GitLab-managed terraform state is stored in the deployment project on ops.gitlab.net to decouple deployment from gitlab.com.The canonical deployment project is still able to run execute dry-run jobs by using the `RUNWAY_DEPLOY_HOST_PRAT` and `RUNWAY_DEPLOY_HOST_PROJECT_ID`. This allows reconciler to execute `gitlab-terraform` commands configured to the remote backend on ops.gitlab.net.

Refer to [this issue](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/361) for more background context.
