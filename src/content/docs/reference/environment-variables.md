---
title: Environment Variables
description: This page detials how to use environment variables in Runway
---

This page outlines the **Environment Variables** available to Runway services.

We differentiate between two types of environment variables: *Platform Variables* and *Service Variables*.

For configuration best practices, refer to [guide](/welcome/best-practices).

## Constraints

Environment variables cannot use the same name as [secrets](/guides/secrets-management), otherwise an overlap error will be raised.

## Platform Variables

**Platform Variables** are defined by the platform, i.e. Runway. They can be *dynamic*, e.g. holding information about
the region in which the application is running.

### Cloud Run

Runway uses Cloud Run to execute services, which define a few environment variables. Most notably:

* `PORT`

  The port your HTTP server should listen on.

Reference: https://cloud.google.com/run/docs/container-contract#env-vars

### Runway

Runway defines the following *Platform Variables:*

* `RUNWAY_REGION`

  The GCP region in which the application is running, e.g. `us-east1`.

## Service Variables

**Service Variables** can be set in two ways: by adding a YAML file to your repository, and by adding a secret to Vault.

### Repository

Environment variables are defined in two deployment environment specific YAML files:

* `.runway/env-production.yml` for the "Production" environment.
* `.runway/env-staging.yml` for the "Staging" environment.

**WARNING**: Do _not_ store secrets as plain text in your repository. See [Exposing Secrets to a Runway
Service](/guides/secrets-management/) for guidance on storing and accessing secrets.

#### Format

Environment variables must be represented as double-quoted strings in YAML, e.g. `"staging"`, `"8080"`, `"true"`, etc.
Environment variables must be use `KEY: VALUE` convention, e.g. `FOO: "BAR"`.

**Example:** The following file configures the environment variable `EXAMPLE_API_URL` for the "Staging" environment of a
service.

```yaml
# .runway/env-staging.yml
---
EXAMPLE_API_URL: "https://example.com/api/v1"
```

**Note:** To set the same value in the "Production" environment, possibly to a different value, use the
`.runway/env-production.yml` file.

#### Changes

*Service Variables* are coupled with a deployment. That means that any changes to environment variables will only take
effect after a deployment.
