---
title: Service Manifest
description: How to configure infrastructure in Runway
---

Runway allows service owners to configure infrastructure using a service manifest hosted in service project. The service manifest is considered primary interface for operating platform. To maintain stable interfaces, Runway will avoid breaking changes whenever possible.

## Example

```yaml
# .runway/runway.yml
spec:
  # omitted for brevity
  image: my-image:my-tag
  container_port: 8080
  regions:
    - us-east1
    - europe-west1
```

## Schema

The Runway service manifest uses [JSON Schema](https://json-schema.org/) for documentation and validation. The schema is defined in [`manifest.schema.json`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/blob/main/schemas/service-manifest/v1/manifest.schema.json?ref_type=heads) in the [runwayctl](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/) repository.

## Documentation

The Runway service manifest schema is used to generate static documentation. For documentation, refer to `runwayctl` [pages](https://schemas.runway.gitlab.com/RunwayService/).

## Validation

The Runway service manifest schema is used to validate `🔍 Validate Runway manifest` job of `validate` stage in service project CI:

```
✅ .runway/runway.yml: Valid
```

Additionally, deployment jobs are validated.

## Versioning

The Runway service manifest schema version is pinned at `"runway/v1"`. To avoid breaking changes, attributes are introduced in backwards-compatible format. Not to be confused with Runway [releases](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/releases), which use semantic versioning.

The Runway service manifest also has a `"runway/v1+dev"` where developmental features are configured. When the features are deemed completed, they will be ported into the `"runway/v1"` manifest. To use
developmental features, set `apiVersion` in the Runway manifest to `"runway/v1+dev"`.

As Runway platform matures to [GA](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1123), schema manifest versioning will eventually be supported.

## Environment

The Runway service manifest will deploy identical configuration across staging and production environments by default. As a best practice, staging should mirror production as much as possible.

As a service owner, you have the ability to optionally specify staging configuration that differs from production configuration. Common use cases include under provisioning staging infrastructure resources for cost efficiency, enabling new experimental or beta features, or
derisking production infrastructure changes.

To configure service manifest in staging environment, add `runway-staging.yml` to service project:

```
cp .runway/runway.yml .runway/runway-staging.yml
```

Once `runway-staging.yml` file is created, you can make configuration changes to staging only.
