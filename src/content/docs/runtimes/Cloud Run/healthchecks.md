---
title: Configuring health checks
description: How to configure health checks for your Cloud Run services
---

## Overview

This document touches on health checks for Runway services deployed in Cloud Run.

Runway service owners can define 2 types of healthcheck:

- [startup](https://schemas.runway.gitlab.com/RunwayService/#spec_startup_probe): determines if a container has started and is ready to receive traffic.
- [liveness](https://schemas.runway.gitlab.com/RunwayService/#spec_liveness_probe): determines whether to restart a container. Depends on a successful startup probe.

For information on defining startup and liveness probes, refer to the [Runway manifest schema](https://schemas.runway.gitlab.com/RunwayService/#spec_startup_probe).
You may also refer to [Cloud Run's guide](https://cloud.google.com/run/docs/configuring/healthchecks) for more information and GCP recommended best practices.

NOTE: All Cloud Run services have a default TCP startup probe which tries to open a TCP connection on the container port.

Example of services using HTTP probes
- [ai-gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/blob/8fc90814f4755b44002e2f1af1fd09916d9652e3/.runway/runway.yml#L24)
- [Runway's example-service](https://gitlab.com/gitlab-com/gl-infra/platform/runway/example-service/-/blob/129bdd0f6cdea85b4c824486f19541f033b1634d/.runway/runway-staging.yml#L23)

Example of services using gRPC probes
- [secret detection](https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/-/blob/4f40107e7a4cdfc1c2bfbb5bff42ef2feea693d6/.runway/runway.yml#L21)
