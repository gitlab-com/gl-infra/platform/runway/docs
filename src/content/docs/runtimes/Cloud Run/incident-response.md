---
title: Incident Response
description: Playbook for common incident response processes
---
This page outlines processes that help with incident response for Runway services.

## Restart Service

Cloud Run does not allow us to restart individual or all instances of a service. If you need to restart all instances, e.g. to purge data cached in memory, you need to deploy a new "dummy" release. See [Emergency Deployment](#emergency-deployment).

## Emergency Deployment

Runway supports multiple [Deployment Strategies](/guides/deployment-strategy/).
We recommend to use the normal deployment strategy when possible, since it is the best tested and most familiar process.

That said, there are cases when the default deployemnt process is broken or too slow.
In such situations the `expedited` deployment strategy can help to deploy a new version as quickly as possible.

To enable the `expedited` deployment strategy, add the following to the `.gitlab-ci.yml` file of your *service repo*:

```yaml
variables:
  RUNWAY_DEPLOYMENT_STRATEGY: expedited
```

Adding the variable via `.gitlab-ci.yml` has several advantages:

* The emergency deployment is triggered by the push.
* The setting is more easily discoverable.
* There is a paper trail who added the variable and when.
