---
title: RunwayJob
sidebar:
  order: 2
---

import { Aside } from '@astrojs/starlight/components';

This type of workload deploys a [Cloud Run
Job](https://cloud.google.com/run/docs/overview/what-is-cloud-run#cloud-run-jobs).
Jobs can be triggered on a [schedule](#schedule) and/or [on-demand](#on-demand) (manually triggered CI job).

You can only define a single job per Runway workload. If you need to define
multiple jobs, you will have to create a new Runway service in the
provisioner for every job.

## Example

```yaml title="runway.yml"
apiVersion: runway/v1
kind: RunwayJob
metadata:
  owner_email_handle: ...
  department: ...
  department_group: ...
  product_category: ...
spec:
  region: us-central1
  command:
    - /path/to/command
  args:
    - arg1
    - arg2
  schedule:
    cron: "1 6,14,22 * * *"
```

Unlike [services](/runtimes/cloud-run/services/) (which can be deployed to multiple
regions), jobs can only be deployed and executed in a single `region` (default: `us-east1`).

Refer to the [schema](https://schemas.runway.gitlab.com/RunwayJob/) for all
supported configuration options.

## Schedule

To configure a job to run on a schedule, simple set the `spec.schedule.cron`
attribute to any valid
[crontab](https://man7.org/linux/man-pages/man5/crontab.5.html) compatible
string as shown in the example above.

To pause a cronjob, simply set `spec.schedule.paused` to `true`:

```yaml title="runway.yml" {9}
apiVersion: runway/v1
kind: RunwayJob
metadata:
  ...
spec:
  ...
  schedule:
    cron: "1 6,14,22 * * *"
    paused: true
```

## On-demand

To have the ability to trigger a job on-demand, you will need to add a job to
your `.gitlab-ci.yml` similar to the following:

```yaml title=".gitlab-ci.yml"
include:
  - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
    file: 'ci-tasks/service-project/runway.yml'
    inputs:
      runway_service_id: example-job-431wm0
      image: "$CI_REGISTRY_IMAGE/example-service:${CI_COMMIT_SHORT_SHA}"
      runway_version: v3.40.0

Trigger Example Job - staging:
  extends: .[example-job-431wm0] Execute Job staging

Trigger Example Job - production:
  extends: .[example-job-431wm0] Execute Job production
```

Replace `example-job-431wm0` with your Runway service ID for your desired job.

You will see two new jobs appear in your pipelines:

![Trigger jobs for on-demand jobs](../../../../assets/img/jobs-adhoc-trigger-jobs.png)

To actually trigger these jobs manually, you need to expand the downstream jobs
and click on the play button:

![Run downstream job](../../../../assets/img/jobs-adhoc-run.png)

### Overriding job arguments

If you want to be able to trigger on-demand jobs on your Docker image with
different arguments, you can make use of the `RUNWAY_JOB_OVERRIDE_ARGS`
variable and avoid creating a Runway service for each job that you want to
trigger with different arguments:

Instead of:

```yaml title=".gitlab-ci.yml"
include:
  - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
    file: 'ci-tasks/service-project/runway.yml'
    inputs:
      runway_service_id: foo-db-maintenance
      image: "$CI_REGISTRY_IMAGE/app:${CI_COMMIT_SHORT_SHA}"
      runway_version: v3.40.0
  - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
    file: 'ci-tasks/service-project/runway.yml'
    inputs:
      runway_service_id: foo-user-maintenance
      image: "$CI_REGISTRY_IMAGE/app:${CI_COMMIT_SHORT_SHA}"
      runway_version: v3.40.0

Trigger DB Maintenance - staging:
  extends: .[foo-db-maintenance] Execute Job staging

Trigger User Maintenance - staging:
  extends: .[foo-user-maintenance] Execute Job staging
```

```yaml title=".runway/foo-db-maintenance/runway.yml"
apiVersion: runway/v1
kind: RunwayJob
metadata:
  ...
spec:
  ...
  command:
    - my-app
  args:
    - db-maintenance
```

```yaml title=".runway/foo-user-maintenance/runway.yml"
apiVersion: runway/v1
kind: RunwayJob
metadata:
  ...
spec:
  ...
  command:
    - my-app
  args:
    - user-maintenance
```

... you can achieve the same thing using `RUNWAY_JOB_OVERRIDE_ARGS` and just _one_
service:

```yaml title=".gitlab-ci.yml"
include:
  - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
    file: 'ci-tasks/service-project/runway.yml'
    inputs:
      runway_service_id: foo-maintenance
      image: "$CI_REGISTRY_IMAGE/app:${CI_COMMIT_SHORT_SHA}"
      runway_version: v3.40.0

Trigger DB Maintenance - staging:
  extends: .[foo-maintenance] Execute Job staging
  variables:
    RUNWAY_JOB_OVERRIDE_ARGS: ["db-maintenance"]

Trigger User Maintenance - staging:
  extends: .[foo-maintenance] Execute Job staging
  variables:
    RUNWAY_JOB_OVERRIDE_ARGS: ["user-maintenance"]
```

```yaml title=".runway/foo-maintenance/runway.yml"
apiVersion: runway/v1
kind: RunwayJob
metadata:
  ...
spec:
  ...
  command:
    - my-app
```

<Aside type="danger" title="Not supported for scheduled jobs">
Overriding of job arguments is only supported for on-demand job triggering. If
you want to configure multiple scheduled jobs with different arguments, you
still need to define individual Runway workloads (one for each scheduled job) and
configure their command/args in the `runway.yml`.
</Aside>

## Limitations

- Jobs do not have access to network connectivity to GitLab internal-facing
  endpoints. For example: you cannot run a job that hits the alert manager
  endpoint as this is an internal-only facing endpoint.

- By default, each task runs for a maximum of **10 minutes** (configurable via
  `spec.timeout` in seconds), however the max value for the timeout (i.e.,
  maximum runtime allowed) is 24 hours (`86400` seconds).

## Links

- Google documentation for [best
practices](https://cloud.google.com/run/docs/jobs-retries) around error-handling.
