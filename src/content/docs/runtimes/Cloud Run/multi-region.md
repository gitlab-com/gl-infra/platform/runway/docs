---
title: Multi-Region
description: How to use multiple regions in Runway
sidebar:
  order: 3
---

## Regions

Runway provides functionality to provision and serve traffic from multiple regions in GCP [Cloud Run](https://cloud.google.com/run/docs). GCP [Global Load Balancer](https://cloud.google.com/load-balancing/docs/https) will automatically route requests to nearest geographical backend for regional Cloud Run services.

### Default

By default, Runway services are provisioned and deployed to GCP [region](https://cloud.google.com/compute/docs/regions-zones) `us-east1` in South Carolina, North America.

### Supported

Runway supports all Cloud Run [locations](https://cloud.google.com/run/docs/locations). New available regions are occasionally announced in Cloud Run [release notes](https://cloud.google.com/run/docs/release-notes).

## Setup

### Prerequisites

To get started, upgrade Runway version to [v2.26.0](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/releases/v2.26.0) or later:

```yaml
# .gitlab-ci.yml
include:
  # omitted for brevity
  - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
    file: 'ci-tasks/service-project/runway.yml'
    inputs:
      runway_service_id: example-service
      image: "$CI_REGISTRY_IMAGE/example-service:${CI_COMMIT_SHORT_SHA}"
      runway_version: v2.26.0
```

### Configuration

Regions must be specified in two places: in the *Service Inventory* and in the *Service Manifest*.

#### Service Inventory

The service inventory is located in Runway [provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) project.
To provision a multi-region service, set the [`regions` attribute](https://gitlab-com.gitlab.io/gl-infra/platform/runway/provisioner/inventory.schema.html#inventory_items_regions) of your service's entry in [`inventory.yml`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml?ref_type=heads).
This field needs to hold all regions you plan to use for both, the `production` and the `staging` environments.

**Example:**

```yaml
# inventory.yml
- name: example-service
  project_id: 46267196
  regions:
    - europe-west1
    - us-east1
```

This will provision region-specific infrastructure, such as package repositories, for your service.

#### Service Manifest

The Service Manifest is located in your service project in the `.runway/` directory.
To deploy a multi-region service, set the [`regions` attribute](https://schemas.runway.gitlab.com/RunwayService/#spec_regions) in `runway.yml`:

```yaml
# .runway/runway.yml
spec:
  # omitted for brevity
  image: "$CI_REGISTRY_IMAGE/example-service:${CI_COMMIT_SHORT_SHA}"
  regions:
    - europe-west1
    - us-east1
```

If provided, the [`regions` attribute](https://schemas.runway.gitlab.com/RunwayService/#spec_regions) must contain at least one region.
After merging and deploying Service Manifest changes, confirm the deployment job has succeeded:

```bash
Apply complete! Resources: 2 added, 3 changed, 1 destroyed.
Outputs:
stable_revisions = {
  "europe-west1" = "example-service-abcd"
  "us-east1" = "example-service-abcd"
}
```

Terraform output is considered an internal interface and may be subject to unannounced breaking changes.

## Regional Service

Runway service owners may require updating service application code to become regionally aware for cross-region downstream dependencies, e.g. in-memory caches, managed services, etc. Below are available options for region context.

### Metadata Server

**Recommended**: Cloud Run instances expose a metadata server to retrieve region by performing HTTP request to `http://metadata.google.internal/computeMetadata/v1/instance/region`. For more information, refer to Cloud Run [documentation](https://cloud.google.com/run/docs/container-contract#metadata-server).

### Environment Variable

*→ Reference: [Environment Variables](/reference/environment-variables/)*

Runway sets the `RUNWAY_REGION` environment variable to the GCP region of the service.

This environment variable is provided for convenience. The metadata server is considered the authoritative source of
truth.

## Observability

By default, Runway provides [observability](/reference/observability/) for default and custom metrics. For multi-region services, metrics are made regionally aware.

### Default Metrics

For services using [default metrics](/reference/observability/#default), Runway will automatically add `region` label based on `location` label.

### Custom Metrics

For services using [custom metrics](/reference/observability/#custom), Runway will automatically add `region` label to scrape configuration for `otel-collector` sidecar container running in regional Cloud Run services.

## Known Issues

### Deprovision Region

Right now, automatically deprovisioning region is not supported. Refer to [#111](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/111).

As workaround, refer to [gcloud command](https://github.com/hashicorp/terraform-provider-google/issues/6376#issuecomment-1218572715) for manual deprovisioning without downtime.

## Support

To report a bug, open new issue in Runway [issue tracker](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues).

## References

* [GCP Cloud Run Multiple Regions](https://cloud.google.com/run/docs/multiple-regions)
* [GCP External Global Load Balancer](https://cloud.google.com/load-balancing/docs/https)
* [GCP Serverless NEGs](https://cloud.google.com/load-balancing/docs/https/setting-up-https-serverless)
