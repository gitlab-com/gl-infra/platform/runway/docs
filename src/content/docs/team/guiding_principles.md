---
title: Guiding Principles
description: These are the guiding principles that allow the Runway team to decentralise decision making
---


## Principles

1. Use native GCP tools if they are "good enough"
1. Contributions welcome
    - We welcome outside contributions and strive to make this an easy experience
1. No Snowflakes
    - Runway leverages economies of scale. We achieve this through standardization and avoiding one-off customizations.
1. Iterations are as small as possible, not necessarily small
1. Keep complexity to a minimum
    -    Prefer simple solutions that solve an existing problem over complex "elegant" solutions that solve problems that might surface in the future. Delete unused code.
1. The interface is stable
    - If we change the way Runway works, it is on us to provide a migration path or do the migration ourselves.
1. Customer-usability
    - Design features with Self-Managed (cloud native) and Dedicated in mind.
