---
title: Runway Maintainership
description: How to become a maintainer for Runway engineering projects
---

Runway is an internal platform as a service (PaaS) actively seeking maintainers for [engineering projects](https://handbook.gitlab.com/handbook/engineering/projects/).

## Projects

| **Name** | **Location** |
|---|---|
| [Reconciler](https://handbook.gitlab.com/handbook/engineering/projects/#runway-reconciler) | [gitlab-com/gl-infra/platform/runway/runwayctl](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl) |
| [Provisioner](https://handbook.gitlab.com/handbook/engineering/projects/#runway-provisioner) | [gitlab-com/gl-infra/platform/runway/provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) |

## Domain Expertise

To maintain Runway, the following technologies are primarily used:

* [Go](https://go.dev/) for programming language.
* [Terraform](https://www.terraform.io/) for infrastructure as code.
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) for deployment component templates.
* [JSON Schema](https://json-schema.org/) for service manifest validation and documentation.
* [GCP](https://cloud.google.com/run) for cloud infrastructure.

Additionally, the following technologies comprise of entire platform:

* [Kubernetes](https://kubernetes.io/) for container orchestration.
* [Helm Charts](https://helm.sh/) for observability package management.
* [Jsonnet](https://jsonnet.org/) for service/metrics catalog.
* [Grafana](https://grafana.com/grafana/dashboards/) for dashboards.
* [Prometheus](https://prometheus.io/) for metrics.

If you are familiar with the domain expertise above, or are interested in learning by doing, please consider becoming a maintainer.

## How to become a maintainer

As of July 2024, Runway is considered a [smaller project](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#maintainership-process-for-smaller-projects) with less than 10 internal contributors. As a result, Runway currently uses an [accelerated onboarding process](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#accelerated-maintainer-onboarding-for-smaller-projects).

### Criteria

1. Read documentation (eg architecture, guides, strategy)
1. Author minimum of 3 MRs (eg `feat:`, `fix:`)
1. Review minimum of 3 MRs
1. Onboard minimum of 1 service project
1. Perform minimum of 1 service project version upgrade

### Process

To become a trainee maintainer:

1. Open maintainer onboarding issue using [template](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/new?issuable_template=maintainer)
1. Update [team member profile](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) projects (eg `runway-reconciler: trainee_maintainer`)

To become a maintainer:

1. Complete maintainer onboarding issue
1. Update [team member profile](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) projects (eg `runway-reconciler: maintainer`)
1. Request MR approval from Runway EM, PM, and at least 1 existing maintainer
1. That's it! 🎉 You're officially a Runway maintainer
