---
title: Best practices
description: How to follow best practices for Runway services
---

A guide for best practices to design, development, and operate a service on Runway.

## Configuration

Configuration is seperate from application code and is used to specify application settings across environment deployments. Service owners have the following options for [configuration](https://12factor.net/config):

1. [Environment Variables](/reference/environment-variables)
1. [Secrets Management](/guides/secrets-management)

Environment variables are checked into source control in your service project and secrets are securely injected during runtime.

| **Environment Variable Examples** | **Secrets Example**   |
|-----------------------------------|-----------------------|
| Ports                             | API Tokens            |
| Endpoints                         | Passwords             |
| Public Information                | Sensitive Information |

## Container Security

Runway's currently supported runtime of GCP Cloud Run supports [sandboxes](https://cloud.google.com/run/docs/securing/security#compute-security) for execution environments. Runway deploys services using the default (i.e., first generation) execution environments. To further improve container security, run your container as a **non-root** [user](https://docs.docker.com/reference/dockerfile/#user).

During [production readiness review](https://handbook.gitlab.com/handbook/engineering/infrastructure/production/readiness/), AppSec requires containers to run with least privilege as a non-root user.
