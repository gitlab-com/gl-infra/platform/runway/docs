---
title: Offboarding
description: How to safely remove your Runway workload and its provisioned resources
sidebar:
  order: 2
---

## Background

Every Runway workload has resources provisioned by:

1. The deployment project through reconciler; and
1. The provisioner

To safely and completely offboard a Runway workload, resources provisioned by reconciler need to be destroyed first before provisioner
can archive the deployment project and destroy the reconciler service account.

The offboarding workflow is built in the provisioner's merge request workflow.

## As a Runway service owner

Remove the Runway service from the [`inventory.yml` file in the Provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml) and open a merge request.

For power users, the alternative is to set a project variable `DESTROY_RUNWAY_WORKLOAD` as `EVERYTHING` or
your Runway workload name (i.e. deployment project name) and trigger a pipeline on the service project.
Setting the variable to `EVERYTHING` will trigger pipelines on all deployments, should your service project deploy
multiple Runway workloads. If so, set the variable to the desired Runway workload name to target a specific workload for removal.

## As a Runway maintainer

As part of the MR review process, the Runway maintainer is responsible for verifying that the dry-run pipelines
for workload removals on the deployment projects are as expected. The maintainer needs to trigger the removal pipelines
using a manual job before the MR can be safely merged.

Refer to the [workload removal instructions in the README](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner#runway-workload-removal)
for more information on how to safely remove a Runway workload.
