---
title: Getting Started
description: How to onboard onto the Runway Platform
sidebar:
  order: 1
---

import { Tabs, TabItem, Steps, FileTree, Code, Aside } from '@astrojs/starlight/components';

## What is Runway?

**Runway** is a platform to streamline the deployment of projects outside of the main GitLab codebase. If you have a
separate codebase containing a service or a web application that can be distributed as a docker container, **Runway**
can help you easily deploy it to *staging* and *production* environments. It's in development right now but is already
usable for simple deployment scenarios.

## How do I start using Runway?

### Add service to inventory

File an MR to add a service to the
[inventory.yml](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml)
file in the
[provisioner](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner)
project.

<Tabs>
  <TabItem label="Single deployment">

  ```yaml title="inventory.yml"
  - name: example-workload
    project_id: 123456789 # GitLab Project ID
    members:
      - gitlab-username
    groups:
      - gitlab-group
    regions:
      - asia-northeast3
      - us-east1
  ```
  </TabItem>
  <TabItem label="Multiple deployments">
  If you are using multiple deployments from the one service repository, then you will need to add as many
  entries as deployments you need all referencing the **same project ID**. Example:

  ```yaml title="inventory.yml"
  - name: foo-frontend
    project_id: 123456789
    members:
      - gitlab-username
    groups:
      - gitlab-group
  - name: foo-backend
    project_id: 123456789
    members:
      - gitlab-username
    groups:
      - gitlab-group
  ```

  </TabItem>
</Tabs>

| Parameter Name        | Required? | Description                                                                                                                                                                                                                                   |
| --------------------- | :-------: | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `name`                |     X     | The name of the application to deploy. It can be different from the project name.                                                                                                                                                             |
| `project_id`          |     X     | Numerical ID of the GitLab project containing the service's code or package.                                                                                                                                                                  |
| `members`             |           | A list of GitLab usernames to grant `maintainer` access to the deployment project.                                                                                                                                                            |
| `groups`              |           | A list of GitLab groups to grant `maintainer` access to the deployment project. Inherited group members (from parent groups) will be included.                                                                                                |
| `regions`             |           | A list of GCP regions that the Runway service would deploy to. This list is the union of regions for all environments. If left empty, it defaults to \["us-east1"\]. Ensure subnets are configured for the regions in the `networks` section. |
| `exclude_name_suffix` |           | By default, Runway services have a random suffix to avoid name clashes. Only set to `true` for production services. All other services, e.g. test services, should set `false` (or omit the field).                                           |
| `gcp_project_group`   |           | Name of GCP project group this service belongs to. GCP project groups are defined under `gcp_project_groups` at the root level of the inventory file.                                                                                         |

There are two ways the service project could be set up:

<Tabs>
  <TabItem label="Runway config in service repository">
  Using this setup, every commit to the default branch of the service repository will trigger a Runway deployment pipeline in the deployment project.
  The `image` can be configured with the `CI_COMMIT_SHORT_SHA` since the pipeline is triggered from the service project where the application Docker image is built.

  Example: https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist
  </TabItem>
  <TabItem label="Runway config in separate repository">
  Using this setup, the `image` input variable has to be updated to deploy the new version of the application. This grants the user a higher degree of control over each deployment.

  Example: https://gitlab.com/gitlab-com/gl-infra/cells/topology-service-deployer
  </TabItem>
</Tabs>

<Aside type="tip">
You may have noticed a `gcp_project_group` parameter above. By default, all Runway workloads are deployed into our `gitlab-runway-<environment>` GCP projects. Runway can also be configured so that you can _bring your own GCP folder_
and Runway will create GCP projects in said folder and deploy workloads to them. See the [BYO GCP Folder](/guides/byo-gcp-folder/) page for more info.
</Aside>

### Retrieve name of generated deployment project

Once inventory changes are merged, the following happens:

  1. A deployment GitLab project is created in the
    [Deployments](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments) group, as well as a service account
    for executing deployments.
  1. Deployment details are located in the downstream pipeline in the created project. This pipeline should be triggered
    by the CI/CD pipeline of the project.

For the next step you will need to get the Runway service name of your service, which is the name of the project you can
find in [Deployments](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments). The format will be
`{name}-{hash}`. For example, the service id of a service created in a previous step could be `eval-code-viewer-78do0s`.

<Aside type="note">
If you set `exclude_name_suffix: true` for your Runway workload in the `inventory.yml`, then the Runway service ID will be
_exactly_ what you specified as the `name` (i.e., no `-{hash}` suffix)
</Aside>

### Update job token permissions

The following steps will allow your deployment project access to your service
repository required for Runway to function ([see
docs](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#add-a-group-or-project-to-the-job-token-allowlist)).

<Steps>

1. Open **Settings** for your service project
2. Go to **CI-CD**
3. Open the **Job token permissions** drop-down
4. Click on **Add group or project**
5. Enter your generated deployment project (from the previous step)

</Steps>

### Create Runway configuration files

In the project that holds your code (known as the _service repository_), you will need to create the following files:

#### `runway.yml`

##### Format

The contents of this file depends on the type of workload you are deploying:

- [**RunwayService**](/runtimes/cloud-run/services/): this type of workload deploys a [Cloud Run Service](https://cloud.google.com/run/docs/overview/what-is-cloud-run#services).
  You can configure the service with an external and/or internal load balancer.

  **Example**: `woodhouse`, which listens for Slack API requests and performs actions on them.

- [**RunwayJob**](/runtimes/cloud-run/jobs/): this type of workload deploys a [Cloud Run Job](https://cloud.google.com/run/docs/overview/what-is-cloud-run#cloud-run-jobs), which can
  be configured to trigger on a schedule or on-demand via CI.

  **Example**: `woodhouse-handover`, which creates an EOC handover issue at set times of the day.

##### Location

Reconciler will first check if the path `.runway/<runway service id>/` exists. If it does, it will look for config files
(e.g., `runway.yml`) under this path. If the path does **not** exist, *then* it will search under `.runway/`.

<Aside type="caution" title="Precedence">
Once your repository contains a `.runway/<runway service id>/` directory, Reconciler will look for all config
files under this path. Failure to create the necessary config files under this path will result in an error.
</Aside>

Example tree structure for running a single deployment:

<FileTree>
- .runway
  - runway.yml
  - env-staging.yml
  - env-production.yml
- .gitlab-ci.yml
- ... etc
</FileTree>

Example tree structure for running multiple deployments:

<FileTree>
- .runway
  - runway.yml
  - env-staging.yml
  - env-production.yml
  - foo-backend
    - runway.yml
    - env-staging.yml
    - env-production.yml
- .gitlab-ci.yml
- ... etc
</FileTree>

In the example above, when running a deployment for Runway service ID `foo-backend`, it will use the config files under
the `foo-backend` path, however deployments for any other Runway service ID will use the config files under `.runway/`.

Alternatively, to be explicit, you could also make it look like the following and it will have the same effect with the
exception that deployments for any other Runway service ID **will fail** because there is no `runway.yml` (nor `env-*.yml`)
file in the `.runway/` directory:

<FileTree>
- .runway
  - foo-frontend
    - runway.yml
    - env-staging.yml
    - env-production.yml
  - foo-backend
    - runway.yml
    - env-staging.yml
    - env-production.yml
- .gitlab-ci.yml
- ... etc
</FileTree>

Once you have your config files in the right places, scroll down to read the section on updating your project's
`.gitlab-ci.yml` to trigger multiple deployments.

##### Stage-specific `runway.yml`

If you only have a single `runway.yml` file then **it will be used for all stages**. You can, alternatively, create
different Runway config files for each stage by defining `runway-<stage>.yml`.

Example tree structure might look like this:

<FileTree>
- .runway
    - foo-frontend
      - runway.yml
      - runway-staging.yml
      - env-staging.yml
      - env-production.yml
    - foo-backend
      - runway-production.yml
      - runway-staging.yml
      - env-staging.yml
      - env-production.yml
- .gitlab-ci.yml
- ... etc
</FileTree>

- In the `foo-frontend` service above, the **production** deployment would use `runway.yml` (as there is no `runway-production.yml`) while
  staging would use `runway-staging.yml`.

- In the `foo-backend` service above, we are being explicit by defining a config file for each stage (`runway-staging.yml` and `runway-production.yml`).
  Runway currently supports two stages (`staging` and `production`). It is your choice whether to be explicit or not, however we do recommend
  being explicit purely from a future-proof perspective so that if Runway ever supports more stages, they will not accidentally use
  your `runway.yml` file.

#### `env-staging.yml` and `env-production.yml`

Inside these two files, which can live in `.runway/` or `.runway/<runway service ID>/` depending on whether you are
deploying a single (former) or multiple services (latter), is where you define any environment variables that you wish
to make available to the containers running in either the `staging` or `production` environments.

Given that these files are cleartext and stored in your project repository, you must *not* place any secrets inside
these files. See the documentation on [secrets management](/guides/secrets-management/) for more info.

Example contents of one of these files might be:

```yaml title="env-staging.yml"
FASTAPI_API_PORT: "8080"
GITLAB_URL: "https://staging.gitlab.com"
GITLAB_API_URL: "https://staging.gitlab.com/api/v4/"
```

### Trigger Runway deployments

<Tabs>
  <TabItem label="Single deployment">
  ```yaml title=".gitlab-ci.yml"
  stages:
    - validate
    - runway_staging
    - runway_production

  include:
    - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
      file: 'ci-tasks/service-project/runway.yml'
      ref: v2.21.0
      inputs:
        runway_service_id: eval-code-viewer-78do0s
        image: "$CI_REGISTRY_IMAGE/deploy:$CI_COMMIT_SHORT_SHA"
        runway_version: v2.21.0
  ```
  </TabItem>
  <TabItem label="Multiple deployments">
  ```yaml title=".gitlab-ci.yml"
  stages:
    - validate
    - runway_staging
    - runway_production

  include:
    - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
      file: 'ci-tasks/service-project/runway.yml'
      ref: v2.47.1
      inputs:
        runway_service_id: foo-frontend
        image: "$CI_REGISTRY_IMAGE/deploy:$CI_COMMIT_SHORT_SHA"
        runway_version: v2.47.1 # minimum version which supports multi-deployment
    - project: 'gitlab-com/gl-infra/platform/runway/runwayctl'
      file: 'ci-tasks/service-project/runway.yml'
      ref: v2.47.1
      inputs:
        runway_service_id: foo-backend
        image: "$CI_REGISTRY_IMAGE/deploy:$CI_COMMIT_SHORT_SHA"
        runway_version: v2.47.1 # minimum version which supports multi-deployment
  ```
  </TabItem>
</Tabs>

Ensure you have your config files in the right places by reading the section [above](#runwayyml).

| input name        | description                                                                                                                                                                                                        |
| ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| runway_service_id | The Runway service id of a service that was created in the step 1.                                                                                                                                                 |
| image             | Docker image to be deployed by Runway platform. Note that the image should contain all the parameters required to start your service. Runway also requires you to build this image as part of your pipeline setup. |
| runway_version    | The version of Runway to use. When starting, pick the latest version from the [Releases](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl/-/releases) page.                                        |

#### Check the deployment

Once the code above is integrated into the main branch of your project, you can check `Pipelines` section and see if it was successfully deployed.

![Deployment pipeline](../../../assets/img/onboarding-pipeline-1.png)

If you expand the jobs on the downstream pipelines, you will see a number jobs:

![Downstream pipeline](../../../assets/img/onboarding-pipeline-2.png)

Each of the downstream pipelines corresponds to a deployment to a _stage_. You can see the stage name is part of almost all jobs (e.g., _Deploy **production**_).
You can also appreciate that the `runway_staging` stage (in GitLab terminology) comes before `runway_production` meaning that the staging downstream pipeline has
to pass for production to start. As part of the downstream pipeline jobs, there is a _monitor_ job that runs after the deploy, which performs basic error rate
checks to determine if the service is healthy after the deploy. If the staging _monitor_ job passes, then the production deployment will automatically start.

### Accessing your environment

The best place to find the links to access your environments (once they have been deployed) is through _Operate_ &rarr; _Environments_ in your service project.
In there you should see two environments, `staging` and `production`.
Clicking on the `Open` button will take you to the URL to access that environment:

![Environments](../../../assets/img/onboarding-environments.png)

:::note[SSL Errors]
If you get SSL errors accessing an environment close to when it was deployed for the first time, this is an
unfortunate side effect of the provisioning process for GCP managed SSL certificates. Environments' SSL configuration
can take up to 20 minutes in order to successfully work. This only happens after the first time an environment is deployed.
:::

:::note[Internal services]
If you made the service internally accessible to GitLab Rails, you can access
the service via `<service-id>.internal.runway.gitlab.net` URL format (see below).
:::

#### Public vs Private

By default, your Runway service will be deployed with a public-facing load
balancer accessible via:

- **production**: `<service-id>.runway.gitlab.net`
- **staging**: `<service-id>.staging.runway.gitlab.net`

You can restrict access to your service by deploying it with an internal load balancer instead of an external one. This ensures only GitLab Rails can communicate with your service. Once deployed, your service will be accessible through the following endpoints:

- **production**: `<service-id>.internal.runway.gitlab.net`
- **staging**: `<service-id>.internal.staging.runway.gitlab.net`

This is achieved by modifying your service's `runway.yml` as per the following:

```yaml title="runway.yml" {7-11}
apiVersion: runway/v1
kind: RunwayService
metadata:
  ...
spec:
  ...
  load_balancing:
    external_load_balancer:
      enabled: false # defaults to true
    internal_load_balancer:
      enabled: true  # defaults to false
```

:::tip
To minimize latency, we recommend deploying your workload in the same region (`us-east1`) as the GitLab Rails monolith, since all network traffic through the Internal Load Balancer originates from the Rails side.
:::

### Keeping up with Runway releases

Renovate is a tool that checks for new dependency versions and, if found, will
periodically create MRs to upgrade them to the newest version. It is used
extensively within "Infrastructure".

The easiest way to keep up with Runway releases is to enable [Renovate
CI](https://gitlab.com/gitlab-com/gl-infra/renovate/renovate-ci) triggered merge
requests on your service project by doing the following:

  1. Add `@glrenovatebot` as a `Developer` for your project under `Manage > Members`.
  1. Add `runway-workloads` as a topic in your project under `Settings > General > Naming, description, topics`.
  1. Set `RENOVATE_DISABLED` to `true` as a CI/CD variable if you are currently
     getting MRs filed by
     [`renovate-bot.yml`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.yml)
     from
     [`common-ci-tasks`](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks)

:::note
Ideally we would run Renovate on your service repo using a PrAT or GAT instead
of using Renovate CI and adding `@glrenovatebot` to your service repo, however
this will not work because:

1. Project Access Tokens (PrAT) cannot be added to the deployment project, making it impossible to grant them access.
2. Group Access Tokens (GAT) would have to live in a group that is shared by the deployment project and the service project. That means GATs are technically feasible for projects in `gitlab-com`, but projects in other groups are impossible.
:::

If you're not already using Renovate, once you've added the `runway-workloads`
topic to your service project, [Renovate
CI](https://gitlab.com/gitlab-com/gl-infra/renovate/renovate-ci) will be able to
discover your project and onboard it by opening a merge request [like this
one](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/merge_requests/92).

If you have an existing `renovate.json` file, Renovate will **not** update it
for you so you will need to update the file yourself and add Runway's Renovate config preset
to the list of base configs (i.e. the `extends` keyword). For example:

```json title="renovate.json" {5}
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common",
    "gitlab>gitlab-com/gl-infra/platform/runway/runwayctl:renovate-runway"
  ]
}
```

This will look for patterns like `runway_version: v2.40.0` in your `.gitlab-ci.yml` file and keep the version up to date.

## Troubleshooting

* If your deployment fails for any reason, check the deployment logs.
* If the downstream pipeline failed to be executed, there's probably a permission mismatch.

Currently, a person triggering a deployment must have "Developer" permissions in the
[runwayctl project](https://gitlab.com/gitlab-com/gl-infra/platform/runway/runwayctl) and "Maintainer" permissions in
the generated deployment project
([example](https://gitlab.com/gitlab-com/gl-infra/platform/runway/deployments/eval-code-viewer-78do0s)).
To grant additional permissions, update the `inventory.yml` file in the [provisioner repo](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner) from step 1.

<Aside type="caution">
When forking an existing Runway service project, the `runway_service_id` needs to be updated or the forked
service project will trigger a pipeline to the original service project's deployment project ([example](https://gitlab.com/schin1/ai-assist/-/pipelines/1064584911)).
</Aside>

## Frequently Asked Questions

### How do I roll back the deployment?

At the moment the designed way to roll back a deployment is roll back a commit on the main branch, which will trigger a
new deployment to Runway with the reverted code.

### How do I view metrics?

By default, metrics are reported for all Runway services. To view metrics, you can use `service` filter on [Runway
Service Metrics dashboard](https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics).

**Recommendation**: For additional features, such as custom service overview dashboard, alerts, and capacity planning,
refer to [the observability page](/reference/observability/).

### How do I view logs?

By default, application container logs can be viewed in [Cloud Logging UI](https://cloudlogging.app.goo.gl/wA9NK1ko8QdJ4TGE7) by filtering `resource.labels.service_name` to your `runway_service_id`.

To learn more, refer to [the observability page](/reference/observability/).

### How do I fix Terraform state error?

#### Insufficient permissions

When a pipeline is triggered with insufficient permissions, failed job includes following error message:

```text
Terraform has been successfully initialized!
Acquiring state lock. This may take a few moments...
╷
│ Error: Error acquiring the state lock
│
│ Error message: HTTP remote state endpoint invalid auth
│
│ Terraform acquires a state lock to protect the state from being written
│ by multiple users at the same time. Please resolve the issue above and try
│ again. For most commands, you can disable locking with the "-lock=false"
│ flag, but this is not recommended.
╵
Error: failed to execute terraform command: exit status 1
```

To fix, there are two potential options:

1. Job can be retried by member with the required permissions, or
2. Add your GitLab username to `members` attribute in
   [`inventory.yml`](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/blob/main/inventory.yml?ref_type=heads) and retry
   the job yourself ([example MR](https://gitlab.com/gitlab-com/gl-infra/platform/runway/provisioner/-/merge_requests/32)).
