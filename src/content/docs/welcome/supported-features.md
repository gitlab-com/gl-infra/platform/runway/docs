---
title: Supported Features
description: This describes the features supported on Runway
---

The purpose of this document is to describe the current supported features of Ruwnay. If you have any feedback, please leave it in our [Runway Feedback Issue](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/150)

## Supported Features

The table below describes the currently supported features on the Runway platform.

N.B. A features absence here does not indicated that it will never be included as part of Runway. If you'd like to see a specific use case supported, please reach out to the runway team either in the [`#f_runway`](https://gitlab.slack.com/archives/C05G970PHSA) slack channel or create an issue in the [runway issue tracker](https://gitlab.com/groups/gitlab-com/gl-infra/platform/runway/-/issues).

| Feature | Support |
|---------|---------|
| Container | ✅ |
| Observability | ✅ |
| Postgres | `Planned` |
| Object Storage | ❌ |
| NoSQL/Document DB | ❌ |
| Secrets Management| ✅|
| Cache/MemoryStore | ✅|
| Jobs/CronJobs | ✅ |
| mTLS | `Planned` |


### Observability

| Observability Feature | Configuration Required | Note |
|-----------------------|------------------------|------|
| Metrics | No | Automatically scrapes Stackdriver metrics for services. [Example](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22l5p%22:%7B%22datasource%22:%22mimir-runway%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%20by%20%28service_name%29%20%28stackdriver_cloud_run_revision_run_googleapis_com_request_count%7Bjob%3D%5C%22runway-exporter%5C%22,%20env%3D%5C%22gprd%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-runway%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1). |
| Dashboards | No | Automatically visualize and filter Stackdriver metrics by service name. [Example](https://dashboards.gitlab.net/d/runway-service/runway3a-runway-service-metrics?orgId=1&var-PROMETHEUS_DS=PA258B30F88C30650&var-environment=gprd&var-service=cfeick-dev-zg7kh0). |
| Alerts | Yes | Must have service catalog entry. [Example](https://gitlab.com/gitlab-com/gl-infra/platform/runway/docs/-/blob/master/observability.md?ref_type=heads#setup). |
